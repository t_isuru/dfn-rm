package com.dfn.rm.exam;

import com.dfn.rm.util.Constants;

public abstract class ExamPaperFactory {

    public ExamPaperGenerator getGenerator(int examType, String position) {
        if (examType == Constants.AUTO_GENERATE) {
            switch (position) {
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

}
