package com.dfn.rm.exam;

import com.dfn.rm.beans.ExamPaper;

public interface ExamPaperGenerator {

    ExamPaper generate();

}
