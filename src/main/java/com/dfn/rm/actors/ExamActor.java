package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.ExamEnd;
import com.dfn.rm.actors.messages.ExamExpire;
import com.dfn.rm.actors.messages.ExamStart;
import com.dfn.rm.actors.messages.ScheduleExam;
import scala.concurrent.duration.Duration;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class ExamActor extends AbstractActor {

    private final ScheduleExam scheduleExam;

    public ExamActor(ScheduleExam scheduleExam) {
        this.scheduleExam = scheduleExam;
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        long durationSeconds =
                Math.round((Instant.now().toEpochMilli() - scheduleExam.getExpireDate().getTime()) / 1000.0);
        getContext().system().scheduler().scheduleOnce(
                Duration.create(durationSeconds, TimeUnit.SECONDS),
                getSelf(),
                new ExamExpire(),
                getContext().system().dispatchers().lookup("schedule-event-dispatcher"),
                getSelf());
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                             .match(ExamStart.class, this::handle)
                             .build();
    }

    private void handle(ExamEnd examEnd) {

    }

    private void handle(ExamStart examStart) {

        getContext().system().scheduler().scheduleOnce(
                Duration.create(scheduleExam.getDuration(), TimeUnit.SECONDS),
                getSelf(),
                new ExamEnd(),
                getContext().system().dispatchers().lookup("schedule-event-dispatcher"),
                getSelf());
    }
}
