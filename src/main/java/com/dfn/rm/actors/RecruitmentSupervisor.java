package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.util.ActorName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RecruitmentSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(RecruitmentSupervisor.class);

    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Recruitment Supervisor");
        getContext().actorOf(Props.create(ExamsSupervisor.class),
                             ActorName.EXAMS_SUPERVISOR.toString());
        getContext().actorOf(Props.create(ClientSupervisor.class),
                             ActorName.CLIENT_SUPERVISOR.toString());
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create().build();
    }
}
