package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.Cancellable;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.ExamStart;
import com.dfn.rm.connector.websocket.ClientUtils;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.SessionDisconnectEvent;
import com.dfn.rm.connector.websocket.events.SessionInfo;
import com.dfn.rm.connector.websocket.events.SessionTimeOutEvent;
import com.dfn.rm.connector.websocket.messages.RequestHeader;
import com.dfn.rm.connector.websocket.messages.RequestMessage;
import com.dfn.rm.connector.websocket.messages.ResponseHeader;
import com.dfn.rm.datastore.RequestHeaderDataStore;
import com.dfn.rm.util.ActorUtils;
import com.dfn.rm.util.Utils;
import com.dfn.rm.util.json.JSONParser;
import com.dfn.rm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.Duration;

public class ClientActor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ClientActor.class);

    private SessionInfo sessionInfo;

    private Cancellable sessionTimeout;

    public ClientActor(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(RequestMessage.class, this::handleRequestMessage)
                .match(SessionTimeOutEvent.class, this::handleSessionTimeOut)
                .build();
    }

    private void handleRequestMessage(RequestMessage requestMessage) {
        LOGGER.trace(requestMessage);
        int messageType = requestMessage.getHeader().getMessageType();
        switch (messageType) {
            case MessageType.REQUEST_MSG_PULSE:
                rescheduleSessionTimeout();
                sendMessageToSession(requestMessage);
                break;
            case MessageType.REQUEST_MSG_START_EXAM:
                handleExamStartEvent(requestMessage);
                break;
            default:
                break;
        }
    }

    private void handleExamStartEvent(RequestMessage requestMessage) {
        try {
            ExamStart examStart = Utils.getMessageInstance(requestMessage.getData(), ExamStart.class);
            ActorUtils.getExamsSupervisor(getContext())
                      .tell(examStart, getSelf());

        } catch (Exception e) {
            ClientUtils.sendErrorResponse(
                    requestMessage.getHeader(),
                    sessionInfo.getSession(),
                    100
            );
        }
    }

    private void sendMessageToSession(Object message) {
        try {
            if (sessionInfo.getSession().isOpen()) {
                sessionInfo.getSession().getRemote().sendString(JSONParser.getInstance()
                                                                          .toJson(message));
            } else {
                LOGGER.error("Trying to write message to a closed session. Message: {}", message);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private void handleSessionTimeOut(SessionTimeOutEvent sessionTimeOutEvent) {
        ActorUtils.getClientSupervisor(getContext())
                  .tell(new SessionDisconnectEvent(sessionInfo), getSelf());
    }

    private void rescheduleSessionTimeout() {
        if (sessionTimeout != null) {
            sessionTimeout.cancel();
        }
        sessionTimeout = getContext().getSystem().scheduler().scheduleOnce(
                Duration.ofSeconds(Settings.getInstance().getClientSettings().getSessionTimeout()),
                getSelf(),
                new SessionTimeOutEvent(),
                getContext().getSystem().dispatcher(),
                getSelf()
        );
    }

    private ResponseHeader getResponseHeader(String responseStoreID) {
        RequestHeader requestHeader = RequestHeaderDataStore.getSharedInstance().getData(responseStoreID);
        if (requestHeader != null) {
            return ResponseHeader.getNewInstance(requestHeader);
        } else {
            return null;
        }
    }


    private void handleSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

}
