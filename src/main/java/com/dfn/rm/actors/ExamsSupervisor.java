package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.impl.fusing.Delay;
import com.dfn.rm.actors.messages.ExamStart;
import com.dfn.rm.actors.messages.ScheduleExam;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.ActorUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;

public class ExamsSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ExamsSupervisor.class);



    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Exam Supervisor");

    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                             .match(ScheduleExam.class, this::handle)
                             .match(ExamStart.class, this::handle)
                             .build();
    }

    private void handle(ScheduleExam scheduleExam) {
        ActorRef examActor = ActorUtils.getChildActor(getContext(),
                                                      ActorName.EXAM_ACTOR,
                                                      scheduleExam.getExamId()
                                                              + "-"
                                                              + scheduleExam.getUserId());
        if(examActor == null) {
            getContext().actorOf(Props.create(ExamActor.class, scheduleExam),
                                 ActorName.EXAM_ACTOR.toString() + "-" + scheduleExam.getExamId()
                                         + "-" + scheduleExam.getUserId());
        }
    }

    private void handle(ExamStart examStart) {
        try {
            sendToExamActor(examStart.getExamId(), examStart.getUserId(), examStart);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void sendToExamActor(long examId, String userId, Object message) {
        ActorRef examActor = ActorUtils.getChildActor(getContext(),
                                                      ActorName.EXAM_ACTOR,
                                                      examId + "-" + userId);
        if(examActor != null) {
            examActor.tell(message, getSender());
        } else {
            LOGGER.error("No exam actor found for message: {}, EID {}, USR {}", message, examId, userId);
        }
    }
}
