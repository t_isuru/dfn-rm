package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import com.dfn.rm.actors.messages.ScheduleExam;
import com.dfn.rm.beans.ExamAgreement;
import com.dfn.rm.beans.ExamDetails;
import com.dfn.rm.beans.User;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.websocket.ClientUtils;
import com.dfn.rm.connector.websocket.EventServer;
import com.dfn.rm.connector.websocket.EventSocket;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.*;
import com.dfn.rm.connector.websocket.messages.*;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.ActorUtils;
import com.dfn.rm.util.Constants;
import com.dfn.rm.util.json.JSONParser;
import com.dfn.rm.util.security.PasswordEncrypter;
import com.dfn.rm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.common.WebSocketSession;
import org.elasticsearch.client.RestHighLevelClient;
import org.jdbi.v3.core.Handle;

import java.util.*;

public class ClientSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ClientSupervisor.class);

    private Map<String, SessionInfo> userIdSessionMap = new HashMap<>();
    private List<Integer> restrictedRequests = new ArrayList<>();
    private RestHighLevelClient elasticsearchClient = null;


    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Client Supervisor");
        int port = Integer.parseInt(Settings.getInstance().getClientSettings().getPort());
        String endpointPath = Settings.getInstance().getClientSettings().getEndpoint();
        List<Integer> restrictedRequestSettings = Settings.getInstance().getClientSettings().getRestrictedRequests();
        restrictedRequests.addAll(restrictedRequestSettings != null ? restrictedRequestSettings : new ArrayList<>());
        try {
            Runnable eventServerThread = () -> EventServer.startWSServer(port, endpointPath);
            new Thread(eventServerThread).start();
            EventSocket.setClientSupervisor(getContext());
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SessionConnectEvent.class, this::handleSessionConnectEvent)
                .match(SessionDisconnectEvent.class, this::handleSessionDisconnectEvent)
                .match(SessionCloseEvent.class, this::handleSessionCloseEvent)
                .match(WSMessageEvent.class, this::handleWSMessageEvent)
                .build();
    }

    private void handleSessionConnectEvent(SessionConnectEvent sessionConnectEvent) {
        LOGGER.info("Session Connected: {}", sessionConnectEvent.getSession());
    }

    private void handleSessionCloseEvent(SessionCloseEvent sessionCloseEvent) {
        LOGGER.warn("Session Closing! : {}", sessionCloseEvent.getSession());
        try {
            List<SessionInfo> sessionInfoList = new ArrayList<>(userIdSessionMap.values());
            for (SessionInfo sessionInfo : sessionInfoList) {
                if (getSessionObjectID(sessionInfo.getSession())
                        .equals(getSessionObjectID(sessionCloseEvent.getSession()))) {
                    cleanUpPreviousSession(sessionInfo.getUserID());
                    // kill session actor if actor alive
                    ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                                 sessionInfo.getUserID());
                    if (actorRef != null) {
                        actorRef.tell(PoisonPill.getInstance(), getSelf());
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleSessionDisconnectEvent(SessionDisconnectEvent sessionDisconnectEvent) {
        try {
            String userID = sessionDisconnectEvent.getSessionInfo().getUserID();
            LOGGER.debug("Session Timed out.: {}", userID);
            cleanUpPreviousSession(userID);
            // kill session actor if actor alive
            ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                         userID);
            if (actorRef != null) {
                actorRef.tell(PoisonPill.getInstance(), getSelf());
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleWSMessageEvent(WSMessageEvent wsMessageEvent) {
        LOGGER.trace("Message Received. WS Message: {}, WS Session: {}", wsMessageEvent.getMessage(),
                     wsMessageEvent.getSession());
        try {
            RequestMessage requestMessage = JSONParser.getInstance().fromJson(wsMessageEvent.getMessage(),
                                                                              RequestMessage.class);
            handleRequestMessage(requestMessage, wsMessageEvent.getSession());
        } catch (Exception e) {
            LOGGER.error("Error while processing the message:{}", wsMessageEvent.getMessage(), e);
        }

    }

    private void handleRequestMessage(RequestMessage requestMessage, Session session) {
        try {
            int messageType = requestMessage.getHeader().getMessageType();
            boolean isSessionValid = true;
            if (messageType == MessageType.REQUEST_MSG_LOGIN) {
                isSessionValid = validateLogin(requestMessage.getHeader());
            } else if (messageType == MessageType.REQUEST_MSG_PULSE) {
                SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
                if (sessionInfo == null) {
                    isSessionValid = false;
                } else {
                    isSessionValid = sessionInfo.isAuthenticated();
                }
            } else if (requestMessage.getHeader().getUserId() == null
                    || requestMessage.getHeader().getUserId().equals("")) {
                isSessionValid = false;
            } else {
                isSessionValid = validateSession(requestMessage.getHeader(), session);
            }
            if (isSessionValid) {
                switch (messageType) {
                    case MessageType.REQUEST_MSG_PULSE:
                        handlePulseMessage(requestMessage);
                        break;
                    case MessageType.REQUEST_MSG_LOGIN:
                        handleLoginRequestMessage(requestMessage, session);
                        break;
                    default:
                        handleClientRequestMessage(requestMessage, session);
                        break;
                }
            } else {
                LOGGER.warn("Invalid request: {}, Session: {}",
                            requestMessage, session);
                ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.UNAUTHORIZED_401);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleClientRequestMessage(RequestMessage requestMessage, Session session) {
        try {
            SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
            ActorRef sessionActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                             sessionInfo.getUserID());
            if (sessionActor != null) {
                sessionActor.tell(requestMessage, getSelf());
                LOGGER.trace("Telling Session Actor: {}",
                             requestMessage);
            } else {
                ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.BAD_REQUEST_400);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handlePulseMessage(RequestMessage requestMessage) {
        SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
        if (sessionInfo != null) {
            ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                         sessionInfo.getUserID());
            if (actorRef != null) {
                actorRef.tell(requestMessage, getSelf());
            }
        }
    }

    private void handleLoginRequestMessage(RequestMessage requestMessage, Session session) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            LoginRequest loginRequest = ClientUtils.getMessageInstance(requestMessage.getData(), LoginRequest.class);
            LoginResponse loginResponse = new LoginResponse();
            ResponseHeader responseHeader = ResponseHeader.getNewInstance(requestMessage.getHeader());

            User user = DBHandler.getInstance().getUser(loginRequest.getUsrname(), handle);

            // check login info is valid
            if (validateLogin(loginRequest, user)) {
                // clean up previous session
                cleanUpPreviousSession(loginRequest.getUsrname());

                // create session info object
                String sessionID = generateSessionID();
                requestMessage.getHeader().setSessionId(sessionID);
                SessionInfo sessionInfo = new SessionInfo(sessionID, session);
                sessionInfo.setClientIP(requestMessage.getHeader().getClientIp());
                sessionInfo.setUserID(loginRequest.getUsrname());
                sessionInfo.setTenantCode(requestMessage.getHeader().getTenantCode());
                sessionInfo.setCommVersion(requestMessage.getHeader().getClientVersion());

                // create session actor
                ActorRef clientActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                                loginRequest.getUsrname());
                if (clientActor == null) {
                    getContext().actorOf(Props.create(ClientActor.class, sessionInfo, elasticsearchClient)
                                              .withDispatcher("client-dispatcher"),
                                         ActorName.Client_ACTOR.toString()
                                                 + "-"
                                                 + loginRequest.getUsrname());
                }

                if (clientActor != null) {
                    clientActor.tell(sessionInfo, getSelf());
                }

                loginResponse.setUserType(user.getUserType());
                loginResponse.setName(user.getFullName());

                boolean l2Validated = true;

                // populate response params
                if (user.getUserType() == Constants.USER_CAT_EXAM) {
                    List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                            loginRequest.getUsrname(), handle
                    );
                    if (examDetailsList.size() > 0) {
                        ExamDetails examDetails = examDetailsList.get(0);
                        ExamAgreement examAgreement = new ExamAgreement(
                                examDetails.getPosition(),
                                examDetails.getDuration(),
                                "DFN-" + examDetails.getExamId(),
                                Constants.DEF_EXAM_DESCRIPTION,
                                Constants.DEF_EXAM_HONOR_CODE
                        );
                        loginResponse.setExamAgreement(examAgreement);

                        ScheduleExam scheduleExam = new ScheduleExam(examDetails.getExpireTime(),
                                                                     examDetails.getDuration(),
                                                                     examDetails.getExamId(),
                                                                     user.getUserName(),
                                                                     examDetails.getExamType(),
                                                                     examDetails.getPosition());
                        ActorUtils.getExamsSupervisor(getContext())
                                  .tell(scheduleExam, getSelf());

                    } else {
                        l2Validated = false;
                    }
                }

                userIdSessionMap.put(loginRequest.getUsrname(), sessionInfo);
                sessionInfo.setUserType(user.getUserType());
                responseHeader.setUserId(user.getUserName());
                responseHeader.setSessionId(sessionID);

                if (l2Validated) {
                    sessionInfo.setAuthenticated(true);
                    loginResponse.setAuthStatus(Constants.LOGIN_SUCCESS);
                } else {
                    sessionInfo.setAuthenticated(false);
                    loginResponse.setAuthStatus(Constants.LOGIN_RESTRICTED);
                }


            } else {
                loginResponse.setAuthStatus(Constants.LOGIN_FAILED);
                session.close();
                ActorRef childActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                               loginRequest.getUsrname());
                if (childActor != null) {
                    childActor.tell(PoisonPill.getInstance(), getSelf());
                }
            }

            ResponseMessage responseMessage = new ResponseMessage();
            responseMessage.setHeader(responseHeader);
            responseMessage.setResObject(loginResponse);
            sendMessage(responseMessage);
        } catch (Exception e) {
            LOGGER.error("Error: ", e);
            ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.BAD_REQUEST_400);
        }
    }

    private void sendMessage(ResponseMessage responseMessage) {
        ActorRef sessionActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                                                         responseMessage.getHeader().getUserId());
        if (sessionActor != null) {
            sessionActor.tell(responseMessage, getSelf());
            LOGGER.debug("Telling Session Actor: {}", sessionActor);
        } else {
            LOGGER.error("Session Actor not found! {}", responseMessage.getHeader().getUserId());
        }
    }

    private boolean validateLogin(RequestHeader requestHeader) {
        boolean isValid = true;
        SessionInfo sessionInfo = userIdSessionMap.get(requestHeader.getUserId());
        if (sessionInfo != null) {
            isValid = !sessionInfo.isAuthenticated();
        }
        return isValid;
    }

    private boolean validateSession(RequestHeader requestHeader, Session session) {
        boolean isValid;
        SessionInfo sessionInfo = userIdSessionMap.get(requestHeader.getUserId());
        if (sessionInfo == null) {
            isValid = false;
        } else {
            isValid = sessionInfo.isAuthenticated();
            if (!requestHeader.getSessionId().equals(sessionInfo.getSessionID())) {
                isValid = false;
            }
            if (!requestHeader.getClientIp().equals(sessionInfo.getClientIP())) {
                isValid = false;
            }
            if (restrictedRequests.contains(requestHeader.getMessageType())) {
                isValid = sessionInfo.getUserType() == Constants.USER_CAT_ADMIN;
            }
        }
        return isValid;
    }

    private void cleanUpPreviousSession(String userID) {
        SessionInfo sessionInfo = userIdSessionMap.get(userID);
        if (sessionInfo != null) {
            // remove session
            userIdSessionMap.remove(userID);
            // close old session
            if (sessionInfo.getSession().isOpen()) {
                sessionInfo.getSession().close();
            }

        }

    }

    private boolean validateLogin(LoginRequest loginRequest, User user) {
        boolean isValid = true;
        if (user == null) {
            isValid = false;
        } else {
            if (!user.getPasswordHash()
                     .equals(PasswordEncrypter
                                     .getsha256Securepassword(loginRequest.getPassword(),
                                                              user.getSalt().getBytes()))) {
                isValid = false;
            }
        }
        return isValid;
    }

    private String getSessionObjectID(Session session) {
        return ((WebSocketSession) session).getConnection().getId();
    }

    private String generateSessionID() {
        return (UUID.randomUUID()).toString();
    }
}
