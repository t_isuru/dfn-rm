package com.dfn.rm.actors.messages;

import java.util.Date;

public class ScheduleExam {

    private Date expireDate;
    private long duration;
    private long examId;
    private String userId;
    private int examType;
    private String position;

    public ScheduleExam(Date expireDate, long duration, long examId, String userId, int examType, String position) {
        this.setExpireDate(expireDate);
        this.setDuration(duration);
        this.setExamId(examId);
        this.setUserId(userId);
        this.setExamType(examType);
        this.setPosition(position);
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getExamId() {
        return examId;
    }

    public void setExamId(long examId) {
        this.examId = examId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getExamType() {
        return examType;
    }

    public void setExamType(int examType) {
        this.examType = examType;
    }
}
