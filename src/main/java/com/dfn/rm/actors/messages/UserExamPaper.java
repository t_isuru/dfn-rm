package com.dfn.rm.actors.messages;

import com.dfn.rm.beans.ExamPaper;

public class UserExamPaper {

    private long examId;
    private String userId;
    private ExamPaper examPaper;

    public UserExamPaper(long examId, String userId, ExamPaper examPaper) {
        this.setExamId(examId);
        this.setUserId(userId);
        this.setExamPaper(examPaper);
    }

    public long getExamId() {
        return examId;
    }

    public void setExamId(long examId) {
        this.examId = examId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ExamPaper getExamPaper() {
        return examPaper;
    }

    public void setExamPaper(ExamPaper examPaper) {
        this.examPaper = examPaper;
    }
}
