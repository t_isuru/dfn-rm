package com.dfn.rm.actors.messages;

public class ExamStart {

    private long examId;
    private String userId;

    public ExamStart(long examId, String userId) {
        this.setExamId(examId);
        this.setUserId(userId);
    }

    public long getExamId() {
        return examId;
    }

    public void setExamId(long examId) {
        this.examId = examId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
