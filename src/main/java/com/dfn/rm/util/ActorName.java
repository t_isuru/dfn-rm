package com.dfn.rm.util;

/**
 * Enum for storing pre-defined actor names for easy access.
 * CAUTION! - If there are more than one actor of a certain type, such as ExecutionReportWorker,
 * use the enum value with a suffix, such as ExecutionReportWorker-1 to make the Actor name Unique.
 */
public enum ActorName {
    RECRUITMENT_SUPERVISOR("RecruitmentSupervisor"),
    EXAMS_SUPERVISOR("ExamsSupervisor"),
    CLIENT_SUPERVISOR("ClientSupervisor"),
    EXAM_ACTOR("ExamActor"),
    Client_ACTOR("ClientActor");


    /**
     * Actor's name.
     */
    private final String name;

    /**
     * @param name - Actor's name.
     */
    ActorName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
