/**
 * Utility package of Athena Server
 */
package com.dfn.rm.util;

import com.dfn.rm.util.json.JSONParser;

import java.util.Map;
import java.util.UUID;

/**
 * Main Utility class for performing various common util actions.
 */
public final class Utils {

    public static <T extends Object> T getMessageInstance(Object message, Class<T> type) {
        String jsonString = JSONParser.getInstance().toJson(message, Map.class);
        return JSONParser.getInstance().fromJson(jsonString, type);
    }

    public static String getRandomString(int length) {
        return UUID.randomUUID().toString().substring(0, length);
    }

}
