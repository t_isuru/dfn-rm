package com.dfn.rm.util;

/**
 * Exception thrown for AthenaSettings1.yml file not found in the class path.
 */
public class SettingsNotFoundException extends Exception {
}
