package com.dfn.rm.util.settings;

public class Settings {

    private static final Settings INSTANCE = new Settings();

    private DBSettings dbSettings;

    private ClientSettings clientSettings;

    public static Settings getInstance() {
        return INSTANCE;
    }

    public DBSettings getDbSettings() {
        return dbSettings;
    }

    public void setDbSettings(DBSettings dbSettings) {
        this.dbSettings = dbSettings;
    }

    public ClientSettings getClientSettings() {
        return clientSettings;
    }

    public void setClientSettings(ClientSettings clientSettings) {
        this.clientSettings = clientSettings;
    }
}
