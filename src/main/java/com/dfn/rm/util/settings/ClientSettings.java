package com.dfn.rm.util.settings;

import java.util.List;

/**
 * Settings class for Server ClientSettings settings of Athena.
 */
public class ClientSettings {
    /**
     * IP address of Server ClientSettings.
     */
    private String ip;

    /**
     * Web Socket Port of Server ClientSettings.
     */
    private String port;

    /**
     * Secure Web Socket port of Server ClientSettings.
     */
    private String securePort;

    /**
     * Client API endpoint.
     */
    private String endpoint;

    private String keystorePath;

    private String keystorePassword;

    private long keepAliveInterval;

    private long sessionTimeout;

    private List<Integer> restrictedRequests;

    private String defaultAdminPassword = "123";

    /**
     * @return - IP address of Server ClientSettings.
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip - Server ClientSettings IP Address.
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return - Port of Server ClientSettings.
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port - Server ClientSettings Port.
     */
    public void setPort(String port) {
        this.port = port;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getKeystorePath() {
        return keystorePath;
    }

    public void setKeystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getSecurePort() {
        return securePort;
    }

    public void setSecurePort(String securePort) {
        this.securePort = securePort;
    }

    /**
     * Secure Web Socket port of Server ClientSettings.
     */
    public long getKeepAliveInterval() {
        return keepAliveInterval;
    }

    public void setKeepAliveInterval(long keepAliveInterval) {
        this.keepAliveInterval = keepAliveInterval;
    }

    public long getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public List<Integer> getRestrictedRequests() {
        return restrictedRequests;
    }

    public void setRestrictedRequests(List<Integer> restrictedRequests) {
        this.restrictedRequests = restrictedRequests;
    }

    public String getDefaultAdminPassword() {
        return defaultAdminPassword;
    }

    public void setDefaultAdminPassword(String defaultAdminPassword) {
        this.defaultAdminPassword = defaultAdminPassword;
    }
}
