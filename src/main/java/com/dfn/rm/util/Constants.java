package com.dfn.rm.util;

public class Constants {

    public static final int USER_CAT_ADMIN = 1;
    public static final int USER_CAT_EXAM = 0;

    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_FAILED = 0;
    public static final int LOGIN_RESTRICTED = 2;

    public static final String DEF_EXAM_DESCRIPTION = "EXAM DESCRIPTION";
    public static final String DEF_EXAM_HONOR_CODE = "EXAM HONOR CODE";


    public static final int AUTO_GENERATE = 0;
    public static final int PERSISTED_GENERATE = 1;

    public static final String POSITION_INTERN = "INTERN";
    public static final String POSITION_SE = "SOFTWARE ENGINEER";
    public static final String POSITION_SSE = "SENIOR SOFTWARE ENGINEER";
    public static final String POSITION_ATL = "ASSOCIATE TECH LEAD";
    public static final String POSITION_TL = "TECH LEAD";
    public static final String POSITION_STL = "Senior TECH LEAD";

}
