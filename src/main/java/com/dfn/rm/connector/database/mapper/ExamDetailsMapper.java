package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.ExamDetails;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

/**
 * Sql data mapper for Algo Order Recovery data.
 */
public class ExamDetailsMapper implements RowMapper<ExamDetails> {

    @Override
    public ExamDetails map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        ExamDetails details = new ExamDetails();
        details.setDuration(resultSet.getLong("details"));
        details.setPosition(resultSet.getString("position"));
        details.setExpireTime(Date.from(Instant.ofEpochMilli(resultSet.getLong("expire_time"))));
        details.setPaperId(resultSet.getString("paper_id"));
        details.setExamId(resultSet.getLong("exam_id"));
        details.setExamType(resultSet.getInt("exam_type"));
        return details;
    }
}
