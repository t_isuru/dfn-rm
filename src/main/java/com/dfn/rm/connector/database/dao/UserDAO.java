package com.dfn.rm.connector.database.dao;

import com.dfn.rm.beans.Question;
import com.dfn.rm.beans.User;
import com.dfn.rm.connector.database.mapper.ExamQuestionMapper;
import com.dfn.rm.connector.database.mapper.UserMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface UserDAO {

    @SqlQuery("SELECT * FROM USER WHERE USERNAME = :USERNAME")
    @UseRowMapper(UserMapper.class)
    User getUser(@Bind("USERNAME") String userName);

}
