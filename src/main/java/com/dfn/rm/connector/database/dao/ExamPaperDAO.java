package com.dfn.rm.connector.database.dao;

import com.dfn.rm.beans.ExamDetails;
import com.dfn.rm.beans.Question;
import com.dfn.rm.connector.database.mapper.ExamDetailsMapper;
import com.dfn.rm.connector.database.mapper.ExamQuestionMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface ExamPaperDAO {

    @SqlQuery("SELECT * FROM QUESTIONS WHERE ID = :QUESTION_ID")
    @UseRowMapper(ExamQuestionMapper.class)
    List<Question> getQuestion(@Bind("QUESTION_ID") int questionId);

    @SqlQuery("SELECT * FROM QUESTIONS, EXAM_PAPER WHERE QUESTIONS.ID = EXAM_PAPER.QUESTION_ID AND PAPER_ID = " +
            ":PAPER_ID")
    @UseRowMapper(ExamQuestionMapper.class)
    List<Question> getQuestionFromPaperId(@Bind("PAPER_ID") int paperId);

    @SqlUpdate("SELECT * FROM ANSWERS WHERE QUESTION_ID = :QUESTION_ID")
    void getAnswers(@Bind("QUESTION_ID") int questionId);

    @SqlQuery("SELECT * FROM EXAMS WHERE USER_ID = :USER_ID AND STATUS = 0")
    @UseRowMapper(ExamDetailsMapper.class)
    List<ExamDetails> getExamDetails(@Bind("USER_ID") String userId);

}
