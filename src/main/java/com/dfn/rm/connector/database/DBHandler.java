package com.dfn.rm.connector.database;

import com.dfn.rm.beans.ExamDetails;
import com.dfn.rm.beans.User;
import com.dfn.rm.connector.database.dao.ExamPaperDAO;
import com.dfn.rm.connector.database.dao.UserDAO;
import org.jdbi.v3.core.Handle;

import java.util.List;

public class DBHandler {

    private static final DBHandler INSTANCE = new DBHandler();

    private DBUtilStore dbUtilStore = DBUtilStore.getInstance();

    private DBHandler() {

    }

    public static DBHandler getInstance() {
        return INSTANCE;
    }

    public User getUser(String username, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        return userDAO.getUser(username);
    }

    public List<ExamDetails> getExamDetails(String username, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getExamDetails(username);
    }

}
