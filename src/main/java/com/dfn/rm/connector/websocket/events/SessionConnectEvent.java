package com.dfn.rm.connector.websocket.events;

import org.eclipse.jetty.websocket.api.Session;

/**
 * Event for connecting a new session.
 */
public class SessionConnectEvent {
    private final Session session;

    public SessionConnectEvent(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

}
