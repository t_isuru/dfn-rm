package com.dfn.rm.connector.websocket;

import com.dfn.rm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;

/**
 * Created by manodyas on 9/8/2017.
 */
public final class EventServer {

    private static final Logger LOGGER = LogManager.getLogger(EventServer.class);

    private EventServer() {

    }

    public static void startWSServer(int wsPort, String wsPath) {
        LOGGER.info("WS Server is Starting...");

        Server server = new Server();

        HttpConfiguration httpConfiguration = new HttpConfiguration();
        httpConfiguration.setSecureScheme("https");
        httpConfiguration.setSecurePort(wsPort);

        HttpConfiguration httpsConfiguration = new HttpConfiguration(httpConfiguration);
        httpsConfiguration.addCustomizer(new SecureRequestCustomizer());

        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setKeyStorePath(Settings.getInstance().getClientSettings().getKeystorePath());
        sslContextFactory.setKeyStorePassword(Settings.getInstance().getClientSettings().getKeystorePassword());

        ServerConnector wsConnector = new ServerConnector(server);
        wsConnector.setPort(wsPort);
        server.addConnector(wsConnector);

        ServerConnector wssConnector
                = new ServerConnector(server, sslContextFactory); // THIS WAS MISSING

        wssConnector.setPort(wsPort + 1);
        server.addConnector(wssConnector);

        // Setup the basic application "context" for this application at "/"
        // This is also known as the handler tree (in jetty speak)
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);

        // Add a websocket to a specific path spec
        ServletHolder holderEvents = new ServletHolder("ws-events", EventServlet.class);
        context.addServlet(holderEvents, wsPath);

        try {
            server.start();
            server.join();
            LOGGER.info("WS Server Started");
            //  addInitialOrders();// adding initial Orders

        } catch (Exception e) {
            LOGGER.error(e);
        }
    }
}
