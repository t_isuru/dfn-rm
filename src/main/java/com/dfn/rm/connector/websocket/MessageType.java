package com.dfn.rm.connector.websocket;

public class MessageType {

    public static final int REQUEST_MSG_PULSE = 3;
    public static final int REQUEST_MSG_LOGIN = 1;

    public static final int REQUEST_MSG_START_EXAM = 50;

}
