package com.dfn.rm.connector.websocket;

import com.dfn.rm.connector.websocket.messages.RequestHeader;
import com.dfn.rm.connector.websocket.messages.ResponseHeader;
import com.dfn.rm.connector.websocket.messages.ResponseMessage;
import com.dfn.rm.util.json.JSONParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;

import java.util.Map;

/**
 * Utility functions for Client handling.
 */
public final class ClientUtils {

    private static final Logger LOGGER = LogManager.getLogger(ClientUtils.class);

    private ClientUtils() {

    }

    public static void sendErrorResponse(RequestHeader requestHeader, Session session, int status) {
        try {
            ResponseMessage responseMessage = new ResponseMessage();
            ResponseHeader responseHeader = ResponseHeader.getNewInstance(requestHeader);
            responseHeader.setStatus(status);
            responseMessage.setHeader(responseHeader);
            session.getRemote().sendString(JSONParser.getInstance().toJson(responseMessage));
        } catch (Exception ex) {
            LOGGER.error("Error occurred while sending message. {}, error: {}, session: {}",
                         requestHeader, ex, session);
        }
    }

    public static <T extends Object> T getMessageInstance(Object message, Class<T> type) {
        String jsonString = JSONParser.getInstance().toJson(message, Map.class);
        return JSONParser.getInstance().fromJson(jsonString, type);
    }
}
