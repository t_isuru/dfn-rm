package com.dfn.rm.connector.websocket.messages;

import com.dfn.rm.beans.ExamAgreement;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isurut on 6/2/2020.
 */
public class LoginResponse {
    @SerializedName("name")
    private String name;

    @SerializedName("userType")
    private int userType;

    @SerializedName("authSts")
    private int authStatus = 0;

    @SerializedName("agreement")
    private ExamAgreement examAgreement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public ExamAgreement getExamAgreement() {
        return examAgreement;
    }

    public void setExamAgreement(ExamAgreement examAgreement) {
        this.examAgreement = examAgreement;
    }

    public int getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(int authStatus) {
        this.authStatus = authStatus;
    }
}
