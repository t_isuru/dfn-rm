package com.dfn.rm.connector.websocket.events;

/**
 * Event for Web Socket connection disconnect.
 */
public class SessionDisconnectEvent {
    private final SessionInfo session;

    public SessionDisconnectEvent(SessionInfo session) {
        this.session = session;
    }

    public SessionInfo getSessionInfo() {
        return session;
    }
}
