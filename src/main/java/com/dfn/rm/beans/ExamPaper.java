package com.dfn.rm.beans;

import java.util.List;

public class ExamPaper {

    private List<Question> questions;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
