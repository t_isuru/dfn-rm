package com.dfn.rm.beans;

import java.util.Date;

public class ExamAgreement {

    private String position;
    private long duration;
    private String description;
    private String honorCode;
    private String examId;
    private Date expireTime;

    public ExamAgreement(String position, long duration, String examId, String description, String honorCode) {
        this.setPosition(position);
        this.setDuration(duration);
        this.setHonorCode(honorCode);
        this.setDescription(description);
        this.setExamId(examId);
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHonorCode() {
        return honorCode;
    }

    public void setHonorCode(String honorCode) {
        this.honorCode = honorCode;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }
}
