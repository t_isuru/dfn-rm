/**
 * The bootstrapper package manages initial configuration and successful initialization of the Server.
 * This package reads the configuration files written by the Administrators of Athena Server and spawns the necessary
 * actors which performs the required tasks.
 * Ex: If a brokerage only wants features such as conditional orders, VWAP, TWAP the bootstrapper will only start
 * necessary actors for those features of Athena Server.
 */
package com.dfn.rm.bootstrap;

import akka.actor.ActorSystem;
import akka.actor.Props;
import com.dfn.rm.actors.RecruitmentSupervisor;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.SettingsNotFoundException;
import com.dfn.rm.util.SettingsReader;
import com.dfn.rm.util.settings.Settings;
import com.typesafe.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Starting point of the Recruitment Manager Server.
 */
public final class StartServer {

    private static final Logger LOGGER = LogManager.getLogger(StartServer.class);

    /**
     * Default settings file for athena server.
     */
    private static final String DEFAULT_SETTINGS_FILE = "config/Settings.yml";

    /**
     * Default settings file for akka configuration.
     */
    private static final String DEFAULT_AKKA_SETTINGS_FILE = "config/Akka.conf";

    /**
     * Default constructor.
     */
    private StartServer() {

    }

    /**
     * Main class.
     *
     * @param args - no args are required to run Athena with YML configuration file.
     */
    public static void main(final String[] args) {

        LOGGER.info("Reading Akka Config from: {}", DEFAULT_AKKA_SETTINGS_FILE);

        try {
            Settings settings = SettingsReader.loadSettings(DEFAULT_SETTINGS_FILE);
            Settings.getInstance().setDbSettings(settings.getDbSettings());
            Settings.getInstance().setClientSettings(settings.getClientSettings());

            Config config = SettingsReader.loadAkkaSettings(DEFAULT_AKKA_SETTINGS_FILE);
            LOGGER.info("Creating Recruitment Manager Actor System: ");
            ActorSystem algoEngine = ActorSystem.create("AlgoEngine", config);
            algoEngine.actorOf(Props.create(RecruitmentSupervisor.class),
                               ActorName.RECRUITMENT_SUPERVISOR.toString());
        } catch (SettingsNotFoundException e) {
            e.printStackTrace();
        }

    }
}
